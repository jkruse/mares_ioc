FROM debian

RUN apt-get update
RUN apt-get install wget make gcc g++ libreadline-dev -y
RUN apt-get install python3 python3-pip -y
RUN apt-get install gfortran -y

WORKDIR /tmp
COPY install.sh ./
RUN chmod +x install.sh
RUN ./install.sh

COPY epics.sh /etc/profile.d/
RUN chmod +x /etc/profile.d/epics.sh
RUN echo ". /etc/profile.d/epics.sh" >> /etc/bash.bashrc
RUN arch | xargs -i@ echo "/usr/local/epics/base/lib/linux-@" > /etc/ld.so.conf.d/epics.conf

#RUN rm epics.sh
RUN rm install.sh

RUN apt-get remove swig
RUN apt-get install swig3.0
RUN ln -s /usr/bin/swig3.0 /usr/bin/swig

ARG PATH=$PATH:/sbin:/usr/local/epics/base/bin/linux-x86_64:/usr/local/epics/extensions/bin/linux-x86_64
ARG EPICS_HOST_ARCH=linux-x86_64
ARG EPICS_BASE=/usr/local/epics/base
ARG EPICS_MODULES=/usr/local/epics/modules
ARG EPICS_EXTENSIONS=/usr/local/epics/extensions
ARG EPICS_LOCAL=/usr/local/epics/local
ARG EPICS_CONFIG=/usr/local/epics/apps/config
ARG EPICS_HOSTNAME=$(hostname | /usr/bin/cut -f1 -d'-')
ARG LINUX_HOSTNAME=$(hostname)
RUN pip3 install pcaspy
RUN pip3 install numpy

COPY geodesics.cpython-37m-x86_64-linux-gnu.so /Solid/
COPY server.py /Solid/ 

CMD python3 /Solid/server.py

