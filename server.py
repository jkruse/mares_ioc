from pcaspy import SimpleServer, Driver
from geodesics import mare_at
from datetime import datetime

PRECISION = 6

LATITUDE  = -22.808
LONGITUDE = -47.052

prefix = 'LNLS:'
pvdb = {
    'MARE' : {
	'desc': 'Valores de North, East e Up para a mare terrestre nas coordenadas do Sirius',
        'prec' : 6,
        'scan' : 600,
        'count' : 3,
    },
}

class mareDriver(Driver):
    def __init__(self):
        super(mareDriver, self).__init__()

    def read(self, reason):
        if(reason == 'MARE'):
            t = datetime.now()
            value = [round(i,PRECISION) for i in
			mare_at(t.year, t.month, t.day, t.hour, t.minute, t.second, LATITUDE, LONGITUDE)]
        else:
            value = self.getParam(reason)
        return value

if __name__ == '__main__':
    server = SimpleServer()

server.createPV(prefix, pvdb)
driver = mareDriver()

while True:
    # process CA transactions
    server.process(0.1)
